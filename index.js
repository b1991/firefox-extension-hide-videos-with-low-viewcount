// Inspired by NiceL https://www.reddit.com/user/N1C3L/ (https://pastebin.com/a9Z1WCXv)

const DEBUG = true;
const MODIFIED_ELEMENTS = [];
const BAD_VIDEO_CLASS_NAME = 'very_very_bad_video_brev';
const NEW_VIDEO_CLASS_NAME = 'very_very_new_video_brev';
const NEW_VIDEO_BACKGROUND_COLOR = '#1A1A1A';

const isSubscriptions = () => location.pathname.startsWith('/feed/subscriptions');
const isChannel = () => location.pathname.startsWith('/@');
const isShorts = () => location.pathname.startsWith('/shorts');
const isNumber = (i) => i >= '0' && i <= '9';
const isSpace = i => i === ' ';
const isSeparator = i => i === '.' || i === ',';

function checkVideo(videoViewsText, timePostedText) {
    if (!videoViewsText?.length) {
        DEBUG && console.log(`~NoVideo (videoViewsText=${videoViewsText}, timePostedText=${timePostedText})`);
        return {};
    }
    if (!timePostedText?.length) {
        DEBUG && console.log(`~ProbablyAShort (videoViewsText=${videoViewsText}, timePostedText=${timePostedText})`);
        return {};
    }

    let hasViewCount = false;
    for (let i = 0; i < videoViewsText.length; i++) {
        if (isNumber(videoViewsText[i])) {
            hasViewCount = true;
            break;
        }
    }

    let hasMoreThan1000Views = false;
    for (let i = 0; i < videoViewsText.length - 2; i++) {
        if (!isNumber(videoViewsText[i]) && isSpace(videoViewsText[i + 1]) && !isNumber(videoViewsText[i + 2])) {
            hasMoreThan1000Views = true;
            break;
        }

        if (isNumber(videoViewsText[i]) && isSeparator(videoViewsText[i + 1]) && isNumber(videoViewsText[i + 2])) {
            hasMoreThan1000Views = true;
            break;
        }
    }

    const isBad = !hasViewCount || !hasMoreThan1000Views;
    const isNew = timePostedText.includes('hour ago') || timePostedText.includes('hours ago');

    DEBUG && isBad && console.log(`~BadVideo: '${videoViewsText}' hasViewCount=${hasViewCount} hasMoreThan1000Views=${hasMoreThan1000Views}`);
    DEBUG && isNew && console.log(`~NewVideo: timePostedText=${timePostedText}`);

    return { isBad, isNew };
}

function markAsBadVideo(el) {
    MODIFIED_ELEMENTS.push(el);
    el.classList.add(BAD_VIDEO_CLASS_NAME);
}
function markAsNewVideo(el) {
    MODIFIED_ELEMENTS.push(el);
    el.classList.add(NEW_VIDEO_CLASS_NAME);
}

function restoreAllModifiedElements() {
    while (MODIFIED_ELEMENTS.length) {
        const el = MODIFIED_ELEMENTS.pop();
        el?.classList.remove(BAD_VIDEO_CLASS_NAME);
        el?.classList.remove(NEW_VIDEO_CLASS_NAME);
    }
}

// ---------------------------------------------------------------------------

function update() {
    if (isShorts() || isChannel() || isSubscriptions()) {
        return;
    }
    
    const checkVideoFromList = (htmlCollection, i) => {
        const el = htmlCollection[i];
        const [videoViewsText, timePostedText] = el.getElementsByClassName('inline-metadata-item style-scope ytd-video-meta-block');

        const { isBad, isNew } = checkVideo(
            videoViewsText?.innerHTML,
            timePostedText?.innerHTML,
        );

        if (isBad) markAsBadVideo(el.parentElement);
        if (isNew) markAsNewVideo(el.parentElement);
    }

    const sidePanelVideos = document.getElementsByClassName('style-scope ytd-compact-video-renderer');
    const mainPageVideos = document.getElementsByClassName('style-scope ytd-rich-item-renderer');
    
    for (let i = 0; i < sidePanelVideos.length; i++) {
        checkVideoFromList(sidePanelVideos, i);
    }

    for (let i = 0; i < mainPageVideos.length; i++) {
        if (mainPageVideos[i].id !== 'content') continue;

        checkVideoFromList(mainPageVideos, i);
    }
}

// ---------------------------------------------------------------------------

const style = document.createElement('style');
style.appendChild(document.createTextNode(`
    .${BAD_VIDEO_CLASS_NAME} {
        filter: blur(3px) grayscale() opacity(50%);
        transition: filter 500ms;
    }
    .${BAD_VIDEO_CLASS_NAME}:hover {
        filter: none;
    }
    .${NEW_VIDEO_CLASS_NAME} {
        box-shadow: 0 0 0 4px ${NEW_VIDEO_BACKGROUND_COLOR};
        border-radius: 8px;
        background: ${NEW_VIDEO_BACKGROUND_COLOR};
    }
`));
document.getElementsByTagName('head')[0].appendChild(style);

document.addEventListener('yt-navigate-finish', () => {
    restoreAllModifiedElements();
    setTimeout(update, 350)
});
window.addEventListener('message', () => setTimeout(update, 200));
window.addEventListener('load', () => setTimeout(update, 200));
window.addEventListener('scrollend', () => setTimeout(update));
window.addEventListener('click', () => setTimeout(update, 200));
